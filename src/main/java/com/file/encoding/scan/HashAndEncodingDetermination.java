package com.file.encoding.scan;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.file.encoding.CharsetDetector;
import com.file.encoding.HashKeyGenerator;
import com.file.encoding.dto.FileInfo;

@Component
public class HashAndEncodingDetermination {

	@Autowired
	HashKeyGenerator hashKeyGenerator;
	

	public FileInfo duplicateEncodingFormatCheck(String eventType, Path child) {

		String[] charsetsToBeTested = { "UTF-8", "UTF-16", "windows-1253", "ISO-8859-7" };
		System.out.println("In duplicateEncodingFormatCheck.");
		String hashKeyValue = null;
		try {
			hashKeyValue = hashKeyGenerator.getCheckSumOfFile(child);
			System.out.println("the file" + child.getFileName() + "Hash key of a file is " + hashKeyValue);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String encodingFormat = new CharsetDetector().detectCharset(new File(child.toString()), charsetsToBeTested)
				.toString();

		return createFileInfo(eventType, child, encodingFormat, hashKeyValue);

	}

	private FileInfo createFileInfo(String eventType, Path fileInfo, String encodingType, String hashKeyValue) {
		FileInfo fileInfomation = new FileInfo();
		fileInfomation.setEncodingType(encodingType);
		fileInfomation.setFilename(fileInfo.getFileName().toString());
		fileInfomation.setFolderPath(fileInfo.toAbsolutePath().toString());
		fileInfomation.setFileActionType(eventType.toString());
		fileInfomation.setFileHashKeyValue(hashKeyValue);
		System.out.println("The file information" + fileInfomation);
		return fileInfomation;

	}

}
