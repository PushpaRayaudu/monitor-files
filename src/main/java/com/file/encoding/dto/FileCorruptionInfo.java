package com.file.encoding.dto;

import java.io.Serializable;

public class FileCorruptionInfo implements Serializable {

	String fileName;
	String fileFolder;
	String  corruptionEventType;
	String httpStatusCode;

	
	/**
	 * @return the corruptionEventType
	 */
	public String getCorruptionEventType() {
		return corruptionEventType;
	}

	/**
	 * @param corruptionEventType the corruptionEventType to set
	 */
	public void setCorruptionEventType(String corruptionEventType) {
		this.corruptionEventType = corruptionEventType;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileFolder
	 */
	public String getFileFolder() {
		return fileFolder;
	}

	/**
	 * @param fileFolder the fileFolder to set
	 */
	public void setFileFolder(String fileFolder) {
		this.fileFolder = fileFolder;
	}

	/**
	 * @return the httpStatusCode
	 */
	public String getHttpStatusCode() {
		return httpStatusCode;
	}

	/**
	 * @param httpStatusCode the httpStatusCode to set
	 */
	public void setHttpStatusCode(String httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	

}
