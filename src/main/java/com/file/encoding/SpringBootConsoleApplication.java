package com.file.encoding;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.EnableKafka;

import com.file.encoding.scan.ScanFileProcess;

@SpringBootApplication
@ComponentScan
@EnableKafka
public class SpringBootConsoleApplication implements ApplicationRunner {

	@Autowired
	ScanFileProcess scanFileProces;

    private static final Logger logger = LoggerFactory.getLogger(SpringBootConsoleApplication.class);

    @Value("${file.name}")
    private String fileName;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootConsoleApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		
        for (String name : args.getOptionNames()){
           System.out.println("arg-" + name + "=" + args.getOptionValues(name));
        
        
        Path dir = Paths.get(args.getOptionValues(name).get(0));
        System.out.println("Dir" +dir);
        		//Paths.get(args.getOptionValues(name));
		try {
			scanFileProces.monitorFileSystem(dir, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        }

       	}

	
}
