package com.file.encoding;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;

import com.file.encoding.dto.FileInfo;






public class DirectoryWatcherExample {
	
	
	@SuppressWarnings("unchecked")
	static <T> WatchEvent<T> cast(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}
	private static Map<WatchKey, Path> keys;


    public static void main(String[] args) throws IOException, InterruptedException {
    	keys = new HashMap<WatchKey, Path>();
    	HashAndEncodingDetermination encodingHashKey = new HashAndEncodingDetermination();
        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path path = Paths.get("C:\\Users\\prayadu\\Desktop\\BOA\\POC\\Samples");
        WatchKey key=path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
        keys.put(key, path);
        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
            	Path dir = keys.get(key);
            	WatchEvent<Path> ev = cast(event);
				Path name = ev.context();
            	Path child = dir.resolve(name);
                System.out.println("Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
                FileInfo fileInfo = encodingHashKey.duplicateEncodingFormatCheck(event.kind().name(),child);
                System.out.println(fileInfo.getEncodingType());
                System.out.println(fileInfo.getFileHashKeyValue());
            }
            key.reset();
        }

        watchService.close();
    }

}
